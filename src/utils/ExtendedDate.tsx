// TODO: verify date string is valid upon creation

/**
 * Extends the functionality of the [JavaScript Date object.](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date)
 */
export default class ExtendedDate extends Date {
  private dateString: string;

  /**
   * Creates an `ExtendedDate` object.
   *
   * @param {string} dateString A string representing a date
   */
  constructor(dateString: string) {
    super(dateString);
    this.dateString = dateString;
  }

  /**
   * This ensures that each date will certainly have a format of YYYY-MM-DD.
   *
   * @param {number} value The value to pad with 0s
   * @returns A string with a '0' in front of the value, if the value is less than 10
   */
  private padValue(value: number): string {
    return value < 10 ? `0${value}` : `${value}`;
  }

  /**
   * Validates that user input actually represents a date.
   *
   * @param {any} date The object to check if it is a date
   * @returns Whether or not the user input is a date
   */
  private isValidDate(date: any) {
    return !isNaN(date);
  }

  /**
   * @returns the year of the date
   */
  getYear(): number {
    return super.getUTCFullYear();
  }

  /**
   * @returns the month of the date
   */
  getMonth(): number {
    return super.getUTCMonth() + 1;
  }

  /**
   * @returns the date of the date
   */
  getDate(): number {
    return super.getUTCDate();
  }

  /**
   * @returns a string representation of a date in the format `YYYY-MM-DD`, if the string passed in to create this object was a valid date. Otherwise, simply returns the string that was passed in.
   */
  getStringRepresentation(): string {
    if (!this.isValidDate(this)) {
      return this.dateString;
    }

    return `${this.getYear()}-${this.padValue(this.getMonth())}-${this.padValue(this.getDate())}`;
  }
}
