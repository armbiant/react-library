import { Route } from './types';

/**
 * Checks if an object is a valid {@link Route} object.
 *
 * @param {any} x The object to check
 * @returns {boolean} Whether or not the object is a valid {@link Route} object
 */
export function isRoute(x: any): x is Route {
  const routeToTest = (x as Route);
  return routeToTest.name !== undefined && routeToTest.paths != undefined;
}
