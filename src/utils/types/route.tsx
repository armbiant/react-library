export type Route = {
  /** The name of the route, to be displayed on the navbar */
  name: string;
  /**
   * An array of URL paths that are associated with this route
   * If there are both static and dynamic paths for a route, the static MUST be the first path listed in this array
   * The paths also need to be in the order of user navigation
   */
  paths: Array<string>;
  /** An optional array of `Routes`, that are displayed in a dropdown menu */
  options?: Array<Route> | undefined;
  /** If this route is displayed in a dropdown menu, if a divider should be shown underneath it */
  divider?: boolean;
  /** If this route corresponds to a {@link NavbarLoginPopover} */
  login?: boolean;
  /**
   * If this route has multiple paths associated with it, the index of the active path from the `paths` array
   * This field will be updated as a user navigates through an application. However, it should almost always
   * be initialized to `0`
   */
  activePathIndex?: number;
}
