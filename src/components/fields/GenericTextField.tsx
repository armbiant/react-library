'use client';

import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';

import { KeyboardEventHandler, useState } from 'react';

export type GenericTextFieldProps = {
  /** The name of the form control to assign to this field */
  fcName: string; // TODO: check if this is needed
  /**
   * The name of the field, displayed on the field's label
   *
   * @defaultValue ''
   */
  name?: string;
  /**
   * The variant this field should be displayed as
   *
   * @defaultValue standard
   */
  variant?: 'standard' | 'outlined';
  /**
   * Callback function to perform an action when the value changes
   *
   * @defaultValue null
   */
  onValueChange?: ((value: string) => void) | null; // TODO: check if this is needed
  /**
   * Callback function to perform an action when a key is pressed while this textbox is focused
   *
   * @defaultValue null
   */
  onKeyDown?: KeyboardEventHandler<HTMLDivElement> | null;
  /**
   * Whether or not this field is required in its form
   *
   * @defaultValue false
   */
  required?: boolean;
  /**
   * Input props as defined [here.](https://mui.com/material-ui/api/text-field/)
   * Provided as a way to define [adornments.](https://mui.com/material-ui/react-text-field/#input-adornments)
   *
   * @defaultValue "\{type: 'text'\}"
   */
  inputProps?: {};
  /**
   * The width of the field
   *
   * @defaultValue 100%
   */
  width?: string;
  /**
   * The value of the field
   *
   * @defaultValue ''
   */
  value?: string;
}

const DEFAULT_PROPS: GenericTextFieldProps = {
  fcName: '',
  name: '',
  variant: 'standard',
  onValueChange: null,
  onKeyDown: null,
  required: false,
  inputProps: {type: 'text'},
  width: '100%',
  value: ''
}

/**
 * Generates a generic text field.
 * Based on [MUI Text Field.](https://mui.com/material-ui/react-text-field/)
 *
 * @param {GenericTextFieldProps} props
 * @example
 * import { GenericTextField } from '@oklahoma-biological-survey/obs-react-lib/nextjs/components';
 * import { useState } from 'react';
 *
 * export function TextField() {
 *   const [query, setQuery] = useState(null);
 *
 *   return (
 *     <GenericTextField fcName='example' variant='outlined' width='50%' onValueChange={setQuery} value={query} />
 *   );
 * }
 */
export default function GenericTextField(props: GenericTextFieldProps) {
  props = {
    ...props,
    variant: !props.variant ? DEFAULT_PROPS.variant : props.variant,
    inputProps: !props.inputProps ? DEFAULT_PROPS.inputProps : props.inputProps,
    width: !props.width ? DEFAULT_PROPS.width : props.width
  };

  const [value, setValue] = useState(props.value ? props.value : '');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    setValue(value);

    if (props.onValueChange) {
      props.onValueChange(value);
    }
  };

  return (
    <>
      <FormControl variant={props.variant} sx={{ mt: 1.5, width: props.width }} required={props.required}>
        <TextField
          id={`generic-field-${props.fcName}`}
          label={props.name}
          value={value}
          variant={props.variant}
          onChange={handleChange}
          onKeyDown={props.onKeyDown}
          required={props.required}
          InputProps={props.inputProps}
        />
      </FormControl>
      <br></br>
    </>
  );
}
