// TODO: document

'use client';

import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';

import { OverridableComponent } from '@mui/material/OverridableComponent';
import { SvgIconTypeMap } from '@mui/material';
import { useState } from 'react';

export type AdornmentIcon = OverridableComponent<SvgIconTypeMap<{}, "svg">> & { muiName: string; };

export type AdornmentIconProps = {
  /** A list of MUI icons to be displayed in an input field. */
  icons: Array<React.ReactElement<AdornmentIcon>>;
}

/**
 * Generates adornment props for a password field. Allows for toggling between a text and password field.
 *
 * @remarks
 * Will be deprecated and removed in a future release.
 *
 * @param {AdornmentIconProps} props
 * @example
 * import Visibility from '@mui/icons-material/Visibility';
 * import VisibilityOff from '@mui/icons-material/VisibilityOff';
 *
 * import { GenericTextField, getPasswordAdornmentProps } from '@oklahoma-biological-survey/obs-react-lib/nextjs/components';
 *
 * export function Example() {
 *   return(
 *     <GenericTextField name='Password' fcName='password' required={true} inputProps={getPasswordAdornmentProps({icons: [<VisibilityOff />, <Visibility />]})} />
 *   );
 * }
 */
export function getPasswordAdornmentProps(props: AdornmentIconProps) {
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  }

  const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  }

  return {
    type: showPassword ? 'text' : 'password',
    endAdornment: (
      <InputAdornment position='end'>
        <IconButton
          aria-label='toggle-password-visibility'
          onClick={handleClickShowPassword}
          onMouseDown={handleMouseDownPassword}
        >
          {showPassword ? props.icons[0] : props.icons[1]}
        </IconButton>
      </InputAdornment>
    )
  };
}

/**
 * Adds an MUI icon to the beginning of a text field.
 *
 * @remarks
 * Will be deprecated and removed in a future release.
 *
 * @param {AdornmentIconProps} props
 *
 * @example
 * import SearchIcon from '@mui/icons-material/Search';
 *
 * import { GenericTextField, getStartAdornmentProps } from '@oklahoma-biological-survey/obs-react-lib/nextjs/components';
 * import { useState } from 'react';
 *
 * export function Example() {
 *   const [query, setQuery] = useState(null);
 *
 *   return (
 *     <GenericTextField fcName='test' variant='outlined' inputProps={getStartAdornmentProps({icons: [<SearchIcon key={0} />]})} onValueChange={setQuery} value={props.query} />
 *   );
 * }
 */
export function getStartAdornmentProps(props: AdornmentIconProps) {
  return {
    type: 'text',
    startAdornment: (
      <InputAdornment position='start'>
        {props.icons[0]}
      </InputAdornment>
    )
  };
}
