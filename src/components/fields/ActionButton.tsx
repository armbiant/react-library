'use client';

import Button from '@mui/material/Button';

export type ActionButtonProps = {
  /** The title to display on the button */
  title: string;
  /** Callback function that is called when the button is clicked */
  onButtonClick: () => void;
}

/**
 * Component that generates a button.
 * Based on [MUI Button.](https://mui.com/material-ui/react-button/)
 *
 * @param {ActionButtonProps} props
 *
 * @example
 * import { ActionButton } from '@oklahoma-biological-survey/obs-react-lib/nextjs/components';
 *
 * export function Main() {
 *   const fn = () => {
 *     // do something
 *   };
 *
 *   return (
 *     <ActionButton title='Click Me!' onButtonClick={fn} />
 *   );
 * }
 */
export default function ActionButton(props: ActionButtonProps) {
  const handleClick = () => {
    props.onButtonClick();
  }

  return (
    <Button variant='contained' type='submit' sx={{ mt: 3 }} onClick={handleClick}>{props.title}</Button>
  )
}
