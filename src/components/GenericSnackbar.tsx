'use client';

import Slide, { SlideProps } from '@mui/material/Slide';
import Snackbar from '@mui/material/Snackbar';
import { forwardRef, useImperativeHandle, useState } from 'react';

export type GenericSnackbarProps = {
  /** Reference to React object */
  ref: React.MutableRefObject<any>;
  /** The message to display on the snackbar */
  message: string;
  /**
   * The duration the snackbar should be displayed
   *
   * @defaultValue 3000
   */
  duration?: number;
}

const DEFAULT_PROPS: GenericSnackbarProps = {
  ref: null,
  message: '',
  duration: 3000
}

export type GenericSnackbarRef = {
  open: () => void;
}

/* istanbul ignore next */
function SlideTransition(props: SlideProps) {
  return <Slide {...props} direction='up' />
}

/**
 * Displays an MUI Snackbar on the user's screen.
 * Based on [MUI Snackbar](https://mui.com/material-ui/react-snackbar/)
 *
 * @param {GenericSnackbarProps} props
 *
 * @example
 * import Button from '@mui/material/Button';
 *
 * import { GenericSnackbar } from '@oklahoma-biological-survey/obs-react-lib/nextjs/components';
 * import { useRef, useState } from 'react';
 *
 * export function Layout() {
 *   const [snackbarMessage, setSnackbarMessage] = useState('');
 *   const snackbarRef = useRef(null);
 *   const message = 'message';
 *
 *   const fn = () => {
 *     setSnackbarMessage(message);
 *   };
 *
 *   return (
 *     <Button onClick={fn}>Click Me!</Button>
 *     <GenericSnackbar ref={snackbarRef} message={snackbarMessage} />
 *   );
 * }
 */
const GenericSnackbar = forwardRef<GenericSnackbarRef, GenericSnackbarProps>((props: GenericSnackbarProps, ref) => {
  const [snackbarOpen, setSnackbarOpen] = useState(false);

  useImperativeHandle(ref, () => ({
    open() {
      setSnackbarOpen(true);
    }
  }));

  props = {
    ...props,
    duration: !props.duration ? DEFAULT_PROPS.duration : props.duration
  };

  return (
    <>
      <Snackbar
        anchorOrigin={{vertical: 'bottom', horizontal: 'center'}}
        autoHideDuration={props.duration}
        TransitionComponent={SlideTransition}
        message={props.message}
        open={snackbarOpen}
        onClose={() => setSnackbarOpen(false)}
      />
    </>
  );
});

export default GenericSnackbar;
