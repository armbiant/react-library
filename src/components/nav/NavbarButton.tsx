'use client';

import Button from '@mui/material/Button';
import Link from 'next/link';

import { Route } from '../../utils/types';
import { alpha } from '@mui/material';

type NavbarButtonProps = {
  route: Route;
  currentRoute: string;
  getPath: (route: Route, currentRoute: string) => string | null;
}

export function NavbarButton(props: NavbarButtonProps) {
  const path = props.getPath(props.route, props.currentRoute);
  const isActive = path === props.currentRoute;

  return (
    <Link href={path} style={{ textDecoration: 'none' }}>
      <Button className={`${isActive ? 'active' : ''}`} key={props.route.name} sx={{ color: '#fff', margin: '0 5px', ':hover': { bgcolor: alpha('#000', 0.3) } }}>
        {props.route.name}
      </Button>
    </Link>
  );
}
