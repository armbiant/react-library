'use client';

import Button from '@mui/material/Button';
import ExpandMore from '@mui/icons-material/ExpandMore';
import Link from 'next/link';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import StyledDivider from '../StyledDivider';

import { Route } from '../../utils/types';
import { Theme, alpha } from '@mui/material';
import { useState } from 'react';

type CustomLinkProps = {
  path: string;
  children: React.ReactNode;
}

type NavbarDropdownOptionsProps = {
  options: Array<Route>;
  theme?: Theme;
}

type NavbarDropdownProps = {
  title: string;
  options: Array<Route>;
  theme?: Theme;
}

export function CustomLink(props: CustomLinkProps) {
  const re = /^http/;

  return (
    re.test(props.path) ?
      <a href={props.path} rel='noopener noreferrer' target='_blank'>{props.children}</a>
    :
      <Link href={props.path}>{props.children}</Link>
  );
}

export function NavbarDropdownOptions(props: NavbarDropdownOptionsProps) {
  return (
    <>
      {props.options.map((option, index) => (
        [
          <CustomLink path={option.paths[0]} key={index}>
            <MenuItem sx={{ color: props.theme?.palette.primary.main }}>
              {option.name}
            </MenuItem>
          </CustomLink>,
          <div key={`${index}-divider`}>
            {option.divider ? <StyledDivider margin={1} color={props.theme?.palette.secondary.main} /> : null}
          </div>
        ]
      ))}
    </>
  );
}

export function NavbarDropdown(props: NavbarDropdownProps) {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  for (const option of props.options) {
    if (option.paths.length !== 1) {
      throw new Error(`Dropdown option '${option.name}' has more than one path defined, which is not allowed.`);
    }
  }

  return (
    <>
      <Button
        id='title'
        aria-controls={open ? 'menu-options' : undefined}
        aria-haspopup='true'
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
        sx={{ color: '#fff', ':hover': { bgcolor: alpha('#000', 0.3) } }}
      >
        {props.title}
        <ExpandMore />
      </Button>
      <Menu
        id='menu-options'
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'title',
        }}
      >
        <NavbarDropdownOptions options={props.options} theme={props.theme} />
      </Menu>
    </>
  );
}
