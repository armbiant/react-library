'use client';

import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import IconButton from '@mui/material/IconButton';
import Login from '../Login';
import Popover from '@mui/material/Popover';

import { useState } from 'react';

export type NavbarLoginPopoverProps = {
  typographyColor?: string;
}

/**
 * Displays a login form in a popover from a navbar.
 *
 * @remarks
 * For internal use only
 */
export default function NavbarLoginPopover(props: NavbarLoginPopoverProps) {
  props = {
    ...props,
    typographyColor: !props.typographyColor ? '#000' : props.typographyColor
  };

  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const openLoginPopover = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  }

  const closeLoginPopover = () => {
    setAnchorEl(null);
  }

  const loginPopoverOpen = Boolean(anchorEl);
  const loginPopoverId = loginPopoverOpen ? 'login-popover' : undefined;

  return (
    <>
      <IconButton aria-describedby={loginPopoverId} sx={{ color: 'white' }} aria-label='login' onClick={openLoginPopover}>
        <AccountCircleIcon />
      </IconButton>
      <Popover
        key={loginPopoverId}
        id={loginPopoverId}
        open={loginPopoverOpen}
        anchorEl={anchorEl}
        onClose={closeLoginPopover}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
      >
        <Login typographyColor={props.typographyColor} />
      </Popover>
    </>
  )
}
