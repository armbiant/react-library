'use client';

import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';

import { Route } from '../../utils/types';
import { isRoute } from '../../utils';
import { useState } from 'react';

type NavbarExpandOptionsProps = {
  routes: Array<Route>;
  index: number | undefined;
}

export function NavbarExpandOptions(props: NavbarExpandOptionsProps) {
  const [open, setOpen] = useState(false);
  const route = props.index !== undefined && isRoute(props.routes[props.index]) ? props.routes[props.index] : null;

  const handleClick = () => {
    setOpen(!open);
  };

  if (route) {
    return (
      <>
        <ListItemButton onClick={handleClick}>
          <ListItemText primary={route.name} key={props.index} />
          {open ? <ExpandLess /> : <ExpandMore />}
        </ListItemButton>
        <Collapse in={open} timeout='auto' unmountOnExit>
          <List component='div' disablePadding>
            {route.options?.map((option, index) => (
              <ListItem key={index}>
                <ListItemButton sx={{ pl: 4 }}>
                  <ListItemText primary={option.name} />
                </ListItemButton>
              </ListItem>
            ))}
          </List>
        </Collapse>
      </>
    )
  } else {
    return null;
  }
}
