'use client';

import Divider from '@mui/material/Divider';

export type StyledDividerProps = {
  /** The width of the divider */
  margin?: number;
  /** The color of the divider */
  color?: string; // TODO: change this to accept a theme, rather than a color
}

/**
 * Component that generates a horizontal divider.
 * Based on [MUI Divider.](https://mui.com/material-ui/react-divider/)
 *
 * @remarks
 * This component will be removed in a future release
 *
 * @param {StyledDividerProps} props
 */
export default function StyledDivider(props: StyledDividerProps = {margin: 3, color: '#000'}) {
  return (
    <>
      <Divider variant='middle' sx={{ margin: props.margin, backgroundColor: props.color, borderBottomWidth: 3, opacity: 0.5 }} />
    </>
  );
}
