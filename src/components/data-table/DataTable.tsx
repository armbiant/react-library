import Box from '@mui/material/Box';

import {
  DataGrid,
  GridColDef,
  GridToolbarColumnsButton,
  GridToolbarContainer,
  GridToolbarExport,
  GridToolbarFilterButton
} from '@mui/x-data-grid';

export type DataTableProps = {
  /** The height of the table's container */
  height: number;
  /**
   * Predicate function telling the table which column in each row is the unique id (if not called id)
   *
   * @param row a single row element
   * @returns a unique id for the row
   */
  getRowIdPredicate: (row: any) => string;
  /** The data for this table */
  data: Array<any>;
  /** An array defining each column. [See documentation](https://mui.com/x/api/data-grid/grid-col-def/) */
  columns: Array<GridColDef>;
  /**
   * Number of records to display on a single page
   *
   * @defaultValue 50
   */
  pageSize?: number;
  /**
   * Whether or not to enable the button that allows for selecting which columns are displayed
   *
   * @defaultValue false
   */
  columnsSelectorEnabled?: boolean;
  /**
   * Whether or not to enable filtering of this table
   *
   * @defaultValue false
   */
  filterEnabled?: boolean;
  /**
   * Whether or not to enable exporting of this table
   *
   * @defaultValue false
   */
  exportEnabled?: boolean;
}

type DataTableToolbarProps = {
  columnsSelectorEnabled: boolean;
  filterEnabled: boolean;
  exportEnabled: boolean;
}

/* istanbul ignore next */
const DEFAULT_PROPS: DataTableProps = {
  height: -1,
  getRowIdPredicate: (row: any) => row,
  data: new Array<any>(),
  columns: new Array<GridColDef>(),
  pageSize: 50,
  columnsSelectorEnabled: false,
  filterEnabled: false,
  exportEnabled: false
}

function DataTableToolbar(props: DataTableToolbarProps) {
  if (!props.columnsSelectorEnabled && !props.filterEnabled && !props.exportEnabled) {
    return null;
  }

  return (
    <GridToolbarContainer>
      {
        props.columnsSelectorEnabled ?
          <GridToolbarColumnsButton />
        : /* istanbul ignore next */ <></>
      }
      {
        props.filterEnabled ?
          <GridToolbarFilterButton />
        : /* istanbul ignore next */ <></>
      }
      {
        props.exportEnabled ?
          <GridToolbarExport />
        : /* istanbul ignore next */ <></>
      }
    </GridToolbarContainer>
  );
}

/**
 * Component that generates a table of data.
 * Based on [MUI X Data grid.](https://mui.com/x/react-data-grid/getting-started/)
 *
 * @param {DataTableProps} props
 *
 * @example
 * import { DataTable, GridColDef } from '@oklahoma-biological-survey/obs-react-lib/nextjs/components/data-table';
 *
 * function Table() {
 *   const data = [
 *     {
 *       id: 0,
 *       field1: 'value1',
 *       field2: 'value2'
 *     },
 *     {
 *       id: 1,
 *       field1: 'value3',
 *       field2: 'value4'
 *     },
 *     {
 *       id: 2,
 *       field1: 'value5',
 *       field2: 'value6'
 *     }
 *   ];
 *
 *   const columns: Array<GridColDef> = [
 *     {
 *       field: 'field1',
 *       headerName: 'Field 1',
 *       width: 150,
 *       hideable: false
 *     },
 *     {
 *       field: 'field2',
 *       headerName: 'Field 2',
 *       width: 150,
 *       hideable: false
 *     }
 *   ];
 *
 *   return (
 *     <DataTable
 *       height={400}
 *       getRowIdPredicate={{ row => row.id }}
 *       data={data}
 *       columns={columns}
 *       filterEnabled={true}
 *       exportEnabled={true}
 *     />
 *   );
 * }
 */
export default function DataTable(props: DataTableProps) {
  props = {
    ...props,
    pageSize: !props.pageSize ? DEFAULT_PROPS.pageSize : props.pageSize,
    columnsSelectorEnabled: !props.columnsSelectorEnabled ? DEFAULT_PROPS.columnsSelectorEnabled : props.columnsSelectorEnabled,
    filterEnabled: !props.filterEnabled ? DEFAULT_PROPS.filterEnabled : props.filterEnabled,
    exportEnabled: !props.exportEnabled ? DEFAULT_PROPS.exportEnabled : props.exportEnabled
  };

  return (
    <Box sx={{ height: props.height }}>
      <DataGrid
        getRowId={props.getRowIdPredicate}
        rows={props.data}
        columns={props.columns}
        pageSize={props.pageSize}
        components={{ Toolbar: DataTableToolbar }}
        componentsProps={{
          toolbar: {
            columnsSelectorEnabled: props.columnsSelectorEnabled,
            filterEnabled: props.filterEnabled,
            exportEnabled: props.exportEnabled
          }
        }}
      />
    </Box>
  );
}
