'use client';

import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';

import { ActionButton, GenericTextField, getPasswordAdornmentProps } from './fields';

type LoginProps = {
  typographyColor?: string; // TODO: change this to accept a theme, rather than a color
  // TODO: add login function callback
}

export default function Login(props: LoginProps) {
  const login = () => {
    console.log('login');
  }

  return (
    <Paper sx={{ m: 3, p: 3 }}>
      <Typography variant='h6' color={props.typographyColor} fontWeight='bold'>Login</Typography>
      <GenericTextField name='Username' fcName='username' required={true} />
      <GenericTextField name='Password' fcName='password' required={true} inputProps={getPasswordAdornmentProps({icons: [<VisibilityOff />, <Visibility />]})} />
      <ActionButton title='Login' onButtonClick={login} />
    </Paper>
  );
}
