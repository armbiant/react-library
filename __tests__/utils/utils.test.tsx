import '@testing-library/jest-dom';

import { ExtendedDate, Route, isRoute } from '../../src/utils';

describe('utils', () => {
  it('determines if an object is a Route or not', () => {
    const route: Route = {
      name: 'test-route',
      paths: ['test-path']
    }

    expect(isRoute(route)).toBeTruthy();
    expect(isRoute('test')).toBeFalsy();
  });
});

describe('extended date', () => {
  const dateString = '2020-04-20';
  const date = new Date(dateString);
  const extendedDate = new ExtendedDate(dateString);

  it('creates an extended date object', () => {
    expect(date).toEqual(extendedDate);
  });

  it('returns the year', () => {
    expect(date.getUTCFullYear()).toEqual(extendedDate.getYear());
  });

  it('returns the month', () => {
    expect(date.getUTCMonth() + 1).toEqual(extendedDate.getMonth());
  });

  it('returns the date', () => {
    expect(date.getUTCDate()).toEqual(extendedDate.getDate());
  });

  it('returns a string representation of the date', () => {
    expect(extendedDate.getStringRepresentation()).toEqual(dateString);
  });

  it('returns the date string passed in, if the date string is not valid', () => {
    const dateString = 'No date found';
    const extendedDate = new ExtendedDate(dateString);

    expect(extendedDate.getStringRepresentation()).toEqual(dateString);
  });
});
