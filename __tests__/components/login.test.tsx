import '@testing-library/jest-dom';
import Login from '../../src/components/Login';
import React from 'react';

import { act } from 'react-dom/test-utils';
import { render, screen } from '@testing-library/react';

describe('Login Component', () => {
  it('renders the login form', () => {
    render(<Login />)

    const usernameInput = screen.getByLabelText(/username/i);
    const passwordInput = screen.getAllByLabelText(/password/i);
    const loginButton = screen.getByRole('button', { name: 'Login' });

    expect(usernameInput).toBeInTheDocument();

    for (const element of passwordInput) {
      expect(element).toBeInTheDocument();
    }

    expect(loginButton).toBeInTheDocument();
  });

  it('password visibility can be toggled', () => {
    render(<Login />)

    const passwordInput = screen.getAllByLabelText(/password/i);

    for (const element of passwordInput) {
      expect(['password', 'button']).toContain(element.getAttribute('type'));
    }

    const toggleVisibility = passwordInput.find(element => element.getAttribute('type') == 'button');

    act(() => {
      if (toggleVisibility) {
        toggleVisibility.click();
      }
    });

    for (const element of passwordInput) {
      expect(['text', 'button']).toContain(element.getAttribute('type'));
    }

    act(() => {
      if (toggleVisibility) {
        toggleVisibility.click();
      }
    });

    for (const element of passwordInput) {
      expect(['password', 'button']).toContain(element.getAttribute('type'));
    }
  });

  // TODO: test login click functionality - should call a function passed by props
});
