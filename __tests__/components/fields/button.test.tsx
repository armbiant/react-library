import '@testing-library/jest-dom';
import React from 'react';

import { ActionButton } from '../../../src/components/fields';
import { act } from 'react-dom/test-utils';
import { render, screen } from '@testing-library/react';

describe('Action Button', () => {
  it('renders a standard action button', () => {
    render(<ActionButton title='Test' onButtonClick={() => null} />)

    const btn = screen.getByRole('button');

    expect(btn.getAttribute('type')).toBe('submit');
  });

  it('performs a button click', () => {
    let value = 'init';

    const click = () => value = 'clicked';

    render(<ActionButton title='Test' onButtonClick={click} />);

    const btn = screen.getByRole('button');

    act(() => {
      btn.click();
    });

    expect(value).toEqual('clicked');
  });
});
