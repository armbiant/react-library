import '@testing-library/jest-dom';
import React from 'react';
import SearchIcon from '@mui/icons-material/Search';

import { GenericTextField, getStartAdornmentProps } from '../../../src/components/fields';
import { fireEvent, render, screen } from '@testing-library/react';

describe('Generic Text Field', () => {
  it('renders a standard generic text field', () => {
    render(<GenericTextField name='Test' fcName='test' />);

    const input = screen.getByLabelText(/test/i);

    expect(input.getAttribute('type') === 'text');
    expect(input.getAttribute('value') == '');
    expect(input.parentElement).toHaveClass('MuiInput-root');
  });

  it('renders an outlined generic text field', () => {
    render(<GenericTextField name='Test' fcName='test' variant='outlined' />);

    const input = screen.getByLabelText(/test/i);
    expect(input.parentElement).toHaveClass('MuiOutlinedInput-root');
  });

  it('renders a required generic text field', () => {
    render(<GenericTextField name='Test' fcName='test' required={true} />);

    const input = screen.getByLabelText(/test/i);
    expect(input.getAttribute('required') !== null);
  });

  it('renders a generic text field with a start adornment', () => {
    render(<GenericTextField name='Start Adornment' fcName='start-adornment' inputProps={getStartAdornmentProps({icons: [<SearchIcon />]})} />);

    const input = screen.getByRole('textbox', {name: 'Start Adornment'});

    expect(input).toHaveClass('MuiInputBase-inputAdornedStart');
  });

  it('renders a generic text field with a provided value', () => {
    render(<GenericTextField name='Test' fcName='test' value='test value' />);

    const input = screen.getByRole('textbox', {name: 'Test'});

    expect(input.getAttribute('value') === 'test value');
  });

  it('ensures that the value is updated on user input', () => {
    render(<GenericTextField name='Test' fcName='test' />);

    const input = screen.getByLabelText(/test/i);

    expect(input.getAttribute('value')).toBe('');

    fireEvent.change(input, {target: {value: 'test value'}});

    expect(input.getAttribute('value')).toBe('test value')
  });

  it('ensures that the parent function is called', () => {
    let value = 'init';

    const valueChange = (inputValue: string) => value = inputValue;

    render(<GenericTextField name='Test' fcName='test' onValueChange={valueChange} />);

    const input = screen.getByLabelText(/test/i);

    expect(input.getAttribute('value')).toBe('');

    fireEvent.change(input, {target: {value: 'test value'}});

    expect(value).toEqual('test value');
  });

  it('renders a text field that listens for keystrokes', () => {
    let value = 'init';
    let enterKeyPressed = false;

    const valueChange = (inputValue: string) => value = inputValue;

    const enterKey = (e: React.KeyboardEvent<HTMLInputElement>) => {
      if (e.key === 'Enter') {
        enterKeyPressed = true;
      }
    };

    render(<GenericTextField name='Test' fcName='test' onValueChange={valueChange} onKeyDown={enterKey} />);

    const input = screen.getByLabelText(/test/i);

    expect(input.getAttribute('value')).toBe('');

    fireEvent.change(input, {target: {value: 'test value'}});
    fireEvent.keyDown(input, {key: 'Enter', code: 13, charCode: 13});

    expect(value).toEqual('test value');
    expect(enterKeyPressed).toBe(true);
  });
});
