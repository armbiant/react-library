import '@testing-library/jest-dom';
import React from 'react';

import { DataTable, GridColDef } from '../../../src/components/data-table';
import { render, screen } from '@testing-library/react';

beforeEach(() => {
  jest.spyOn(console, 'warn').mockImplementation(() => {});
});

describe('Data Table', () => {
  const data = [
    {
      id: 0,
      field1: 'value1',
      field2: 'value1'
    },
    {
      id: 1,
      field1: 'value2',
      field2: 'value2'
    },
    {
      id: 2,
      field1: 'value3',
      field2: 'value3'
    },
    {
      id: 3,
      field1: 'value4',
      field2: 'value4'
    },
    {
      id: 4,
      field1: 'value5',
      field2: 'value5'
    }
  ];

  const columns: Array<GridColDef> = [
    {
      field: 'field1',
      headerName: 'Field 1',
      width: 150,
      hideable: false
    },
    {
      field: 'field2',
      headerName: 'Field 2',
      width: 150,
      hideable: false
    }
  ];

  it('renders a data table', () => {
    render(<DataTable
      height={800}
      getRowIdPredicate={(row) => row.id}
      data={data}
      columns={columns}
    />);

    const grid = screen.getByRole('grid');
    const field1Header = screen.getByRole('columnheader', { name: 'Field 1' });
    const field2Header = screen.getByRole('columnheader', { name: 'Field 2' });
    const rowsPerPageBtn = screen.getByRole('button', { name: 'Rows per page: 50' });

    expect(grid).toBeInTheDocument();
    expect(field1Header).toBeInTheDocument();
    expect(field2Header).toBeInTheDocument();
    expect(rowsPerPageBtn).toBeInTheDocument();
  });

  it('renders a data table with a custom amount of rows per page', () => {
    render(<DataTable
      height={400}
      getRowIdPredicate={(row) => row.id}
      data={data}
      columns={columns}
      pageSize={1}
    />);

    expect(() => screen.getByRole('button', { name: 'Rows per page: 1' })).toThrowError('Unable to find an accessible element with the role "button" and name "Rows per page: 1"');

    const prevBtn = screen.getByRole('button', { name: 'Go to previous page' });
    const nextBtn = screen.getByRole('button', { name: 'Go to next page' });

    expect(prevBtn).toBeInTheDocument();
    expect(nextBtn).toBeInTheDocument();
  });

  it('renders a data table with a columns selector button', () => {
    render(<DataTable
      height={400}
      getRowIdPredicate={(row) => row.id}
      data={data}
      columns={columns}
      columnsSelectorEnabled={true}
    />);

    const selectColumnsBtn = screen.getByRole('button', { name: 'Select columns' });

    expect(selectColumnsBtn).toBeInTheDocument();
  });

  it('renders a data table with a filter button', () => {
    render(<DataTable
      height={400}
      getRowIdPredicate={(row) => row.id}
      data={data}
      columns={columns}
      filterEnabled={true}
    />);

    const filterBtn = screen.getByRole('button', { name: 'Show filters' });

    expect(filterBtn).toBeInTheDocument();
  });

  it('renders a data table with an export button', () => {
    render(<DataTable
      height={400}
      getRowIdPredicate={(row) => row.id}
      data={data}
      columns={columns}
      exportEnabled={true}
    />);

    const exportBtn = screen.getByRole('button', { name: 'Export' });

    expect(exportBtn).toBeInTheDocument();
  });
});
