import '@testing-library/jest-dom';
import React from 'react';

import { GenericSnackbar } from '../../src/components';
import { act } from 'react-dom/test-utils';
import { render, screen, waitFor } from '@testing-library/react';

jest.spyOn(React, 'useRef').mockReturnValue({current: null});

describe('Snackbar', () => {
  jest.setTimeout(10000);
  it('opens a snackbar', async () => {
    const snackbarRef = React.useRef(null);
    render(<GenericSnackbar ref={snackbarRef} message='test message' />);

    act(() => {
      if (snackbarRef.current) {
        // @ts-ignore
        snackbarRef.current.open();
      }
    });

    const presentation = screen.getByRole('presentation');
    const alert = screen.getByRole('alert');
    const message = screen.getByText('test message');

    expect(presentation).toBeInTheDocument();
    expect(alert).toBeInTheDocument();
    expect(message).toBeInTheDocument();

    await waitFor(() => {
      expect(() => screen.getByRole('presentation')).toThrowError('Unable to find role="presentation"');
    }, {
      timeout: 4000
    });
  });

  it('opens a snackbar with a custom duration', async () => {
    const snackbarRef = React.useRef(null);
    render(<GenericSnackbar ref={snackbarRef} message='test message' duration={5000} />);

    act(() => {
      if (snackbarRef.current) {
        // @ts-ignore
        snackbarRef.current.open();
      }
    });

    const presentation = screen.getByRole('presentation');
    const alert = screen.getByRole('alert');
    const message = screen.getByText('test message');

    expect(presentation).toBeInTheDocument();
    expect(alert).toBeInTheDocument();
    expect(message).toBeInTheDocument();

    await waitFor(() => {
      expect(() => screen.getByRole('presentation')).toThrowError('Unable to find role="presentation"');
    }, {
      timeout: 6000
    });
  });
});
