import '@testing-library/jest-dom';
import AbcIcon from '@mui/icons-material/Abc';
import React from 'react';

import { CustomTab } from '../../src/utils';
import { CustomTabs } from '../../src/components';
import { render, screen } from '@testing-library/react';

const tabs: Array<CustomTab> = [
  {
    title: 'Tab 1',
    icon: <AbcIcon />,
    content: <p>tab 1</p>
  },
  {
    title: 'Tab 2',
    icon: <AbcIcon />,
    content: <p>tab 2</p>
  }
]

describe('Custom Tabs', () => {
  it('renders tabs', () => {
    render(<CustomTabs tabs={tabs} />);

    const tablist = screen.getByRole('tablist');
    const tab1 = screen.getByRole('tab', { name: 'Tab 1' });
    const tab2 = screen.getByRole('tab', { name: 'Tab 2' });
    const tabPanel = screen.getByRole('tabpanel', { name: 'Tab 1' });

    expect(tablist).toBeInTheDocument();
    expect(tab1).toBeInTheDocument();
    expect(tab2).toBeInTheDocument();
    expect(tabPanel).toBeInTheDocument();
  })
});
