import '@testing-library/jest-dom';
import React from 'react';

import { Navbar } from '../../../src/components';
import { Route } from '../../../src/utils';
import { act } from 'react-dom/test-utils';
import { render, screen } from '@testing-library/react';
import { useRouter } from 'next/navigation';

let fakePathname = '/taxa/search/main';

jest.mock('next/navigation', () => ({
  useRouter() {
    return {
      push(route: string) {
        fakePathname = route;
      }
    }
  },

  usePathname() {
    return fakePathname;
  }
}));

// Ignore console error messages
console.error = jest.fn();

describe('Navbar', () => {
  const routes: Array<Route> = [
    {
      name: 'route-1',
      paths: ['path-1']
    },
    {
      name: 'route-2',
      paths: ['https://google.com/']
    },
    {
      name: 'route-3',
      paths: ['path-3']
    }
  ];

  it('renders a navbar', () => {
    render(<Navbar name="OBS React Library" shortName='obs-react-lib' routes={routes} />);

    // TODO: split out mobile and desktop nav into their own functional components
    const nav = screen.getAllByRole('navigation').find(navBar => navBar.classList.contains('MuiAppBar-root'));
    const title = screen.getByText('OBS React Library');

    expect(nav).toBeInTheDocument();
    expect(title).toBeInTheDocument();

    const btns = screen.getAllByRole('button');
    const links = screen.getAllByRole('link');

    expect(btns.length).toEqual(links.length + 1);
  });

  it('renders a navbar with a dropdown', () => {
    routes.push({
      name: 'route-4',
      paths: ['path-4'],
      options: [
        {
          name: 'option-1',
          paths: ['option-path-1']
        },
        {
          name: 'option-2',
          paths: ['option-path-2']
        }
      ]
    });

    render(<Navbar name="OBS React Library" shortName='obs-react-lib' routes={routes} />);

    const btns = screen.getAllByRole('button');
    const links = screen.getAllByRole('link');

    expect(btns.length).toEqual(links.length + 2);

    const dropdownBtn = screen.getByRole('button', { name: 'route-4' });

    act(() => {
      dropdownBtn.click();
    });

    const dropdownMenu = screen.getByLabelText('route-4');
    expect(dropdownMenu).toBeInTheDocument();

    routes.pop();
  });

  it('renders a navbar with a login option', () => {
    routes.push({
      name: 'route-4',
      paths: [],
      login: true
    });

    render(<Navbar name="OBS React Library" shortName='obs-react-lib' routes={routes} />);

    const btns = screen.getAllByRole('button');
    const links = screen.getAllByRole('link');

    expect(btns.length).toEqual(links.length + 2);

    const loginNavBtn = screen.getByRole('button', { name: 'login' });

    act(() => {
      loginNavBtn.click();
    });

    const usernameInput = screen.getByLabelText(/username/i);
    const passwordInput = screen.getAllByLabelText(/password/i);
    const loginButton = screen.getByRole('button', { name: 'Login' });

    expect(usernameInput).toBeInTheDocument();

    for (const element of passwordInput) {
      expect(element).toBeInTheDocument();
    }

    expect(loginButton).toBeInTheDocument();
  });

  it('renders a navbar with a route whose path will be a pound sign', () => {
    routes.push({
      name: 'Pound Sign',
      paths: []
    });

    render(<Navbar name="OBS React Library" shortName='obs-react-lib' routes={routes} />);

    const link = screen.getByRole('link', { name: 'Pound Sign' });
    expect(link.getAttribute('href')).toBe('#');

    routes.pop();
  });

  // TODO: add test for logo
});

describe('Dynamic Navbar', () => {
  const router = useRouter();

  let routes: Array<Route> = [
    {
      name: 'Search Taxa',
      paths: ['/taxa/search/main', '/taxa/search/main/*', '/taxa/result/main/*']
    },
    {
      name: 'Map',
      paths: ['/taxa/search/map', '/taxa/search/map/*', '/taxa/result/map/*']
    }
  ];

  const ogRoutes = JSON.parse(JSON.stringify(routes));

  it('renders a navbar with a route that has static and dynamic paths, with a static match', () => {
    render(<Navbar name="OBS React Library" shortName='obs-react-lib' routes={routes} />);

    const searchTaxaLink = screen.getByRole('link', { name: 'Search Taxa' });
    const mapLink = screen.getByRole('link', { name: 'Map' });

    expect(searchTaxaLink.getAttribute('href')).toBe('/taxa/search/main');
    expect(mapLink.getAttribute('href')).toBe('/taxa/search/map');
  });

  it('renders a navbar with a route that has static and dynamic paths, with a dynamic match and backwards navigation', () => {
    router.push('/taxa/search/main/grus');

    const { rerender } = render(<Navbar name="OBS React Library" shortName='obs-react-lib' routes={routes} />);

    const searchTaxaLink = screen.getByRole('link', { name: 'Search Taxa' });
    const mapLink = screen.getByRole('link', { name: 'Map' });

    expect(searchTaxaLink.getAttribute('href')).toBe('/taxa/search/main/grus');
    expect(mapLink.getAttribute('href')).toBe('/taxa/search/map/grus');

    expect(routes[0].paths[0]).toBe('/taxa/search/main');
    expect(routes[0].paths[1]).toBe('/taxa/search/main/grus');
    expect(routes[0].paths[2]).toBe('/taxa/result/main/*');

    expect(routes[1].paths[0]).toBe('/taxa/search/map');
    expect(routes[1].paths[1]).toBe('/taxa/search/map/grus');
    expect(routes[1].paths[2]).toBe('/taxa/result/map/*');

    router.push('/taxa/search/main');
    rerender(<Navbar name="OBS React Library" shortName='obs-react-lib' routes={routes} />);

    expect(searchTaxaLink.getAttribute('href')).toBe('/taxa/search/main');
    expect(mapLink.getAttribute('href')).toBe('/taxa/search/map');

    expect(routes[0].paths[0]).toBe('/taxa/search/main');
    expect(routes[0].paths[1]).toBe('/taxa/search/main/grus');
    expect(routes[0].paths[2]).toBe('/taxa/result/main/*');

    expect(routes[1].paths[0]).toBe('/taxa/search/map');
    expect(routes[1].paths[1]).toBe('/taxa/search/map/grus');
    expect(routes[1].paths[2]).toBe('/taxa/result/map/*');
  });

  it('renders a navbar with a route that has static and dynamic paths, with a dynamic match and in-place navigation', () => {
    router.push('/taxa/search/main/notropis');

    const { rerender } = render(<Navbar name="OBS React Library" shortName='obs-react-lib' routes={routes} />);

    const searchTaxaLink = screen.getByRole('link', { name: 'Search Taxa' });
    const mapLink = screen.getByRole('link', { name: 'Map' });

    expect(searchTaxaLink.getAttribute('href')).toBe('/taxa/search/main/notropis');
    expect(mapLink.getAttribute('href')).toBe('/taxa/search/map/notropis');

    expect(routes[0].paths[0]).toBe('/taxa/search/main');
    expect(routes[0].paths[1]).toBe('/taxa/search/main/notropis');
    expect(routes[0].paths[2]).toBe('/taxa/result/main/*');

    expect(routes[1].paths[0]).toBe('/taxa/search/map');
    expect(routes[1].paths[1]).toBe('/taxa/search/map/notropis');
    expect(routes[1].paths[2]).toBe('/taxa/result/map/*');

    router.push('/taxa/search/main/quercus');
    rerender(<Navbar name="OBS React Library" shortName='obs-react-lib' routes={routes} />);

    expect(searchTaxaLink.getAttribute('href')).toBe('/taxa/search/main/quercus');
    expect(mapLink.getAttribute('href')).toBe('/taxa/search/map/quercus');

    expect(routes[0].paths[0]).toBe('/taxa/search/main');
    expect(routes[0].paths[1]).toBe('/taxa/search/main/quercus');
    expect(routes[0].paths[2]).toBe('/taxa/result/main/*');

    expect(routes[1].paths[0]).toBe('/taxa/search/map');
    expect(routes[1].paths[1]).toBe('/taxa/search/map/quercus');
    expect(routes[1].paths[2]).toBe('/taxa/result/map/*');
  });

  it('renders a navbar with a route that has static and dynamic paths, with a dynamic match and in-place navigation on the second route', () => {
    routes = ogRoutes;

    router.push('/taxa/search/map/grus');

    const { rerender } = render(<Navbar name="OBS React Library" shortName='obs-react-lib' routes={routes} />);

    const searchTaxaLink = screen.getByRole('link', { name: 'Search Taxa' });
    const mapLink = screen.getByRole('link', { name: 'Map' });

    expect(searchTaxaLink.getAttribute('href')).toBe('/taxa/search/main/grus');
    expect(mapLink.getAttribute('href')).toBe('/taxa/search/map/grus');

    expect(routes[0].paths[0]).toBe('/taxa/search/main');
    expect(routes[0].paths[1]).toBe('/taxa/search/main/grus');
    expect(routes[0].paths[2]).toBe('/taxa/result/main/*');

    expect(routes[1].paths[0]).toBe('/taxa/search/map');
    expect(routes[1].paths[1]).toBe('/taxa/search/map/grus');
    expect(routes[1].paths[2]).toBe('/taxa/result/map/*');

    router.push('/taxa/search/map/quercus');
    rerender(<Navbar name="OBS React Library" shortName='obs-react-lib' routes={routes} />);

    expect(searchTaxaLink.getAttribute('href')).toBe('/taxa/search/main/quercus');
    expect(mapLink.getAttribute('href')).toBe('/taxa/search/map/quercus');

    expect(routes[0].paths[0]).toBe('/taxa/search/main');
    expect(routes[0].paths[1]).toBe('/taxa/search/main/quercus');
    expect(routes[0].paths[2]).toBe('/taxa/result/main/*');

    expect(routes[1].paths[0]).toBe('/taxa/search/map');
    expect(routes[1].paths[1]).toBe('/taxa/search/map/quercus');
    expect(routes[1].paths[2]).toBe('/taxa/result/map/*');
  });

  it('renders a navbar with a route that has static and dynamic paths, that will throw an error', () => {
    router.push('/taxa/search/invalid/grus');
    routes.find(route => route.name === 'Search Taxa')!.activePathIndex = undefined;

    try {
      render(<Navbar name="OBS React Library" shortName='obs-react-lib' routes={routes} />);
    } catch (error) {
      expect(error.message).toEqual('Route with name \'Search Taxa\' is configured incorrectly. Please check the \'paths\' array to ensure there will be a match.');
    }
  });
});
