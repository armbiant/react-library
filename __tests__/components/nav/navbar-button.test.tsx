import '@testing-library/jest-dom';
import React from 'react';

import { NavbarButton } from '../../../src/components/nav/NavbarButton';
import { Route } from '../../../src/utils';
import { render, screen } from '@testing-library/react';

const route: Route = {
  name: 'Route Name',
  paths: ['route-path']
}

const getPath = (route: Route, currentRoute: string) => {
  const paths = route.paths;

  switch (paths.length) {
    case 0:
      return '#';
    case 1:
      return paths[0];
  }

  for (const path of paths) {
    // For this to work properly, the static route MUST be placed at the first position of the paths array
    if (currentRoute === path) {
      return path;
    }

    // Strip off the '/*' from the end of the dynamic route
    if (currentRoute.startsWith(path.substring(0, path.length - 2))) {
      return currentRoute;
    }
  }

  throw new Error(`Route with name ${route.name} is configured incorrectly. Please check the 'paths' array to ensure there will be a match.`);
}

describe('Navbar Button', () => {
  it('renders an inactive navbar button', () => {
    render(<NavbarButton route={route} currentRoute='inactive' getPath={getPath} />);

    const button = screen.getByRole('button');

    expect(button.classList).not.toContain('active');
  });

  it('renders an active navbar button', () => {
    render(<NavbarButton route={route} currentRoute='route-path' getPath={getPath} />);

    const button = screen.getByRole('button');

    expect(button.classList).toContain('active');
  });

  it('renders an inactive navbar button, with no path', () => {
    route.paths = [];

    render(<NavbarButton route={route} currentRoute='route-path' getPath={getPath} />);

    const button = screen.getByRole('button');

    expect(button.classList).not.toContain('active');
  });

  it('renders an active navbar button for a static path, when multiple paths are given', () => {
    route.paths = ['/taxa/search/main', '/taxa/search/main/*'];

    render(<NavbarButton route={route} currentRoute='/taxa/search/main' getPath={getPath} />);

    const link = screen.getByRole('link', { name: 'Route Name' });
    const button = screen.getByRole('button');

    expect(link.getAttribute('href')).toBe('/taxa/search/main');
    expect(button.classList).toContain('active');
  });

  it('renders an active navbar button for a dynamic path, when multiple paths are given', () => {
    route.paths = ['/taxa/search/main', '/taxa/search/main/*'];

    render(<NavbarButton route={route} currentRoute='/taxa/search/main/grus' getPath={getPath} />);

    const link = screen.getByRole('link', { name: 'Route Name' });
    const button = screen.getByRole('button');

    expect(link.getAttribute('href')).toBe('/taxa/search/main/grus');
    expect(button.classList).toContain('active');
  });
});
