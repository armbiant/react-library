import '@testing-library/jest-dom';
import React from 'react';

import { NavbarLoginPopover } from '../../../src/components/nav';
import { act } from 'react-dom/test-utils';
import { render, screen } from '@testing-library/react';

describe('Navbar Login Popover', () => {
  it('renders a login popover on a navbar', () => {
    render(<NavbarLoginPopover />);

    const btn = screen.getByRole('button');

    expect(btn).toBeInTheDocument();

    act(() => {
      btn.click();
    });

    const usernameInput = screen.getByLabelText(/username/i);
    const passwordInput = screen.getAllByLabelText(/password/i);
    const loginButton = screen.getByRole('button', { name: 'Login' });

    expect(usernameInput).toBeInTheDocument();

    for (const element of passwordInput) {
      expect(element).toBeInTheDocument();
    }

    expect(loginButton).toBeInTheDocument();
  });
});
