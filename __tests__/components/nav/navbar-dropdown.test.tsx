import '@testing-library/jest-dom';
import React from 'react';

import { CustomLink, NavbarDropdown, NavbarDropdownOptions } from '../../../src/components/nav/NavbarDropdown';
import { Route } from '../../../src/utils';
import { act } from 'react-dom/test-utils';
import { render, screen } from '@testing-library/react';

const options: Array<Route> = [
  {
    name: 'route-1',
    paths: ['path-1']
  },
  {
    name: 'route-2',
    paths: ['https://google.com/']
  },
  {
    name: 'route-3',
    paths: ['path-3']
  }
]

// Ignore console error messages
console.error = jest.fn();

describe('Custom Link', () => {
  it('renders an HTML link to an external page', () => {
    render(<CustomLink path='https://google.com/'><p>test link</p></CustomLink>);

    const link = screen.getByRole('link');
    const p = screen.getByText('test link');

    expect(link.getAttribute('target')).toEqual('_blank');
    expect(p).toBeInTheDocument();
  });

  it('renders a NextJS Link component to an internal page', () => {
    render(<CustomLink path='about'><p>test link</p></CustomLink>);

    const link = screen.getByRole('link');
    const p = screen.getByText('test link');

    expect(link.getAttributeNames().length).toBe(1);
    expect(p).toBeInTheDocument();
  });
});

describe('Navbar Dropdown Options', () => {
  it('renders options in a dropdown list', () => {
    render(<NavbarDropdownOptions options={options} />);

    const links = screen.getAllByRole('link');
    const menuItems = screen.getAllByRole('menuitem');

    expect(links.length).toBe(3);
    expect(menuItems.length).toBe(3);

    for (const link of links) {
      expect(['path-1', 'https://google.com/', 'path-3']).toContain(link.getAttribute('href'));
    }
  });

  it('renders options in a dropdown list, with a divider between two options', () => {
    // @ts-ignore
    options.find(option => option.name === 'route-2').divider = true;

    render(<NavbarDropdownOptions options={options} />);

    const links = screen.getAllByRole('link');
    const menuItems = screen.getAllByRole('menuitem');

    expect(links.length).toBe(3);
    expect(menuItems.length).toBe(3);

    for (const link of links) {
      expect(['path-1', 'https://google.com/', 'path-3']).toContain(link.getAttribute('href'));
    }

    const divider = screen.getByRole('separator');
    expect(divider).toBeInTheDocument();
  });
});

describe('Navbar Dropdown', () => {
  it('renders a dropdown menu in a navbar', () => {
    render(<NavbarDropdown title='Test Dropdown' options={options} />);

    const btn = screen.getByRole('button');
    expect(btn).toBeInTheDocument();

    expect(() => screen.getByLabelText('Test Dropdown')).toThrowError('Unable to find a label with the text of: Test Dropdown');

    act(() => {
      btn.click();
    });

    const dropdownMenu = screen.getByLabelText('Test Dropdown');
    expect(dropdownMenu).toBeInTheDocument();
  });

  it('attempts to render a dropdown menu in a navbar, but will fail because of multiple paths being specified for a single option', () => {
    options.push({
      name: 'route-4',
      paths: ['path-4', 'path-4-2']
    });

    try {
      render(<NavbarDropdown title='Error Dropdown' options={options} />);
    } catch (error) {
      expect(error.message).toEqual('Dropdown option \'route-4\' has more than one path defined, which is not allowed.');
    }
  });
});
