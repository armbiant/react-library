# Changelog

## Version 2.1.0 - February 16, 2023
  - Added
    - Ability to listen for key presses when focused in a `GenericTextField` component

## Version 2.0.5 - February 16, 2023
  - Fixed
    - Bug with route paths being refreshed before an update has been processed

## Version 2.0.4 - February 13, 2023
  - Reverting v2.0.3

## Version 2.0.3 - February 13, 2023
  - Fixed
    - Bug with route paths being refreshed before an update has been processed

## Version 2.0.2 - February 12, 2023
  - Fixed
    - Bug with active routes on navbar not updating properly
  - Documented
    - Still existing bug with route paths being refreshed before an update has been processed

## Version 2.0.1 - February 7, 2023
  - Updated
    - README documentation to instruct users on how to install and use this package

## Version 2.0.0 - February 7, 2023
  - Added
    - Documentation to be deployed to pages site
  - Changed
    - Filenames to follow React conventions

## Version 1.2.0 - February 5, 2023
  - Added
    - `DataTable` component
    - `EsriMap` component
  - Fixed
    - Bug with `ExtendedDate` returning `NaN` if the date string passed in is an invalid date

## Version 1.1.0 - January 25, 2023
  - Added
    - `CustomTab` type
    - `CustomTabs` component
    - `ExtendedDate` class

## Version 1.0.1 - January 14, 2023
  - utils/types/route
    - Added
      - `activePathIndex` attribute to keep track of which path should be active for a given route
  - components/nav/navbar
    - Fixed
      - Bug when multiple routes had more than one path, when they should be linked together
      - Bug with paths not updating correctly when navigating back in the browser's history

## Version 1.0.0 - January 13, 2023
  - Added
    - Support for multiple paths (static or dynamic) to be defined for a single route
      - Changed `path` attribute (with type `string`) to `paths` (with type `Array<string>`) in `Route`
  - Removed
    - `actualPath` as an attribute from `Route`
    - `router` parameter from `NavbarProps`

## Version 0.6.0 - January 12, 2023
  - Updating all components and tests to support NextJS 13+

## Version 0.5.1 - January 1, 2023
  - Added ability to specify a value for `GenericTextField`s

## Version 0.5.0 - January 1, 2023
  - Added `GenericSnackbar` component

## Version 0.4.0 - December 27, 2022
  - Added ability to define a callback function for `onValueChange` in `GenericTextField`

## Version 0.3.3 - December 23, 2022
  - Finally fixing types

## Version 0.3.2 - December 22, 2022
  - Trying to fix types

## Version 0.3.1 - December 20, 2022
  - Adding typings

## Version 0.3.0 - December 19, 2022
  - Adding tests for entire library

## Version 0.2.1 - December 14, 2022
  - Patching CWE-94

## Version 0.2.0 - December 13, 2022
  - Adding unit testing
    - Still a WIP, only have tests for a few components

## Version 0.1.11 - December 6, 2022
  - Changing package name

## Version 0.1.10 - December 6, 2022
  - Fixing build to not have `dist/` in import path

## Version 0.1.9 - December 5, 2022
  - Trying to fix types

## Version 0.1.8 - December 5, 2022
  - Removing exports from types
  - Only publishing `dist/` foler
  - Using hosted runner to publish

## Version 0.1.7 - December 5, 2022
  - Changing order of hooks in Navbar

## Version 0.1.6 - December 5, 2022
  - Adding `useEffect` hook to ensure rehydration does not throw an error

## Version 0.1.5 - December 5, 2022
  - Adding babel plugin for emotion

## Version 0.1.4 - December 5, 2022
  - Fixing hydration issues with Navbar
  - Converting interfaces to types and exporting them

## Version 0.1.3 - December 5, 2022
  - Need to pass `NextRouter` as prop to navbar

## Version 0.1.2 - December 5, 2022
  - Using babel instead of rollup to build

## Version 0.1.1 - November 30, 2022
  - Library isn't working, trying some things to fix it

## Version 0.1.0 - November 28, 2022
  - Initial module definition
