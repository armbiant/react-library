# Oklahoma Biological Survey React Library

This library provides a collection of React components and functions for reuse in multiple applications to achieve a cohesive user interface and improve ease of development. All components are based on [MUI](https://mui.com/).

## Contributing
Contributions to this library are always welcome! Please fork [the repository](https://gitlab.com/oklahoma-biological-survey/obs-react-lib) and submit a merge request with your changes.

## Installation
Ensure you have `npm` installed on your system. [Instructions here](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm).

```
# Add the Oklahoma Biological Survey NPM registry to your local NPM configuration
npm config set @oklahoma-biological-survey:registry "https://gitlab.com/api/v4/projects/41409520/packages/npm/"

# Install the React library
npm install @oklahoma-biological-survey/obs-react-lib
```

## Usage
View documentation of each React component and function [here](https://oklahoma-biological-survey.gitlab.io/obs-react-lib/).

## Feedback
Please report any bugs or feature requests by creating an issue [here](https://gitlab.com/oklahoma-biological-survey/obs-react-lib/-/issues).
